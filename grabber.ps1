﻿


Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Intro" -PercentComplete 1



Write-Output "+==============================================================================="
Write-Output "| ABOUT:"
Write-Output "| This tool helps collecting relevat log entries and configuration"
Write-Output "| to help troubleshoot IIS web application issues."
Write-Output "| The following info and files would collected:"
Write-Output "|  - HTTP.SYS configuration and log files, the HTTERR"
Write-Output "|  - IIS configuration, like applicationHost.config"
Write-Output "|  - Web application configuration, Web.config, for the selected app"
Write-Output "|  - Windows Event Viewer entries like System and Application logs"
Write-Output "|  - IIS log files and optional FREB traces for selected site"
Write-Output "| The script needs administrative access to gather the needed files."
Write-Output "| Find more and alternative at http://linqto.me/iis-basic-files."
Write-Output "+-------------------------------------------------------------------------------"



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Check Administrator rights" -PercentComplete 3



$currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
$userIsAdmin = $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
if (-NOT $userIsAdmin)
{
    Write-Warning " You are NOT running this script as Administrator!"
    Write-Warning " Some of the needed stuff - such as files under C:\Windows\System32\ - would not be collected."
    Write-Warning " Please re-run this script from an administrative console."
    Write-Warning " This tool will now exit. Press ENTER to finish..."
    Read-Host
    exit
}



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Are IIS sites defined?" -PercentComplete 4



$iisSites = Get-WebSite | Sort-Object -Property ID | Select-Object -Property ID,Name
if ((Get-WebSite).Count -LT 1)
{
    Write-Warning " There are no IIS sites defined on this machine."
    Write-Warning " This tool will now exit. Press ENTER to finish..."
    Read-Host
    exit
}



Write-Output "| "
Write-Output "| "
Write-Output "| "
$choice = Read-Host -Prompt "| Please confirm if you want to continue [y/N]"
if (-NOT($choice -EQ "Y")) { break }



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Investigated timeframe" -PercentComplete 5



#==============================================================================
#   Determine how much back in time logs should be collected
#------------------------------------------------------------------------------

$today = Get-Date
$daysBackInTime = 10
$startingDate = $today.AddDays(-$daysBackInTime)
Write-Output "| "
Write-Output "| "
Write-Output "| "
Write-Output "| The tool is only collecting recent log files, by default not older that 10 days."
Write-Output ([string]::Format("| If you want collecting older or more recent than {0:yyyy-MM-dd}...",$startingDate))
Write-Output "| ...choose how many days back in time or hit enter for default [10 days]"
Write-Output "| "
do {
    $choice = Read-Host -Prompt ">   Enter an integer [1-100] or hit enter to continue"
    if ([string]::IsNullOrWhiteSpace($choice))
    {
        $daysBackInTime = 10
        break
    }
    $validInput = [int32]::TryParse( $choice , [ref]$daysBackInTime)
    if(!$validInput)
    {
        Write-Warning "  Only integers please, or simply hit enter for the default 10 days..."
    }
    else 
    {
        if (($daysBackInTime -LT 1) -OR ($daysBackInTime -GT 100))
        {
            $validInput = $false
            Write-Warning "  Only integers between 1 and 100 please, or simply hit enter for the default 10 days..."
        }
    }
  } until ($validInput)
$startingDate = $today.AddDays(-$daysBackInTime)
Write-Output "| "
Write-Output ([string]::Format("| OK, going back as much as {0:yyyy-MM-dd}.",$startingDate))
Write-Output "| "



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Temporary folder for data collection" -PercentComplete 10

  

#==============================================================================
#   Where do we place the temporary folder with the collected logs?
#------------------------------------------------------------------------------

Write-Output "| "
Write-Output "| "
Write-Output "| "
Write-Output "| Where do you want to place the temporary folder with the collected logs?"
Write-Output "| Options:"
Write-Output "|    [C]urrent directory - everything will be collected right here"
Write-Output "|    [D]esktop - create a \Temp_IIS-ConfigAndLogs-{YYYY-MM-DD}\"
Write-Output "|    [S]ystem drive - a temp folder, like C:\Temp_IIS-ConfigAndLogs-{YYYY-MM-DD}\"
Write-Output "|    The default is the current folder."
Write-Output "| Choose destination for output archive..."
$choice = Read-Host -Prompt ">   Current directory, Desktop, or System drive [C/d/s]"
$destinationRoot = Get-Item -Path "."
if ($choice -EQ "D") { $destinationRoot = [Environment]::GetFolderPath("Desktop") }
if ($choice -EQ "S") { $destinationRoot = $Env:SystemDrive }
$destinationTemp = $destinationRoot.FullName + ([string]::Format("\Temp_IIS-ConfigAndLogs-{0:yyyy-MM-dd}\", $today))
Write-Output "| "
Write-Output "| OK, using the following as temporary destination:"
Write-Output "| "
Write-Output ("+   " + $destinationTemp)
Write-Output "| "
if (Test-Path $destinationTemp) { Remove-Item $destinationTemp -Recurse -Force }
New-Item $destinationTemp -ItemType Directory -Force | Out-Null
$fileCollectionLog = $destinationTemp + "Grabber-file-collection.log"
Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "|       Grabber file collection started"
Add-Content -Path $fileCollectionLog -Value "|       -------------------------------"
Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value ([string]::Format("+       {0:yyyy-MM-dd hh:mm:ss}", $today))
Add-Content -Path $fileCollectionLog -Value ("+       " + $destinationTemp)
Add-Content -Path $fileCollectionLog -Value "|"



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Select IIS site being investigated" -PercentComplete 15



#====================================================================
#   Determine the IIS site that is being investigated
#--------------------------------------------------------------------

Write-Output "| "
Write-Output "| "
Write-Output "| "
$siteID = 0
$selectedSite = $null
if ($iisSites.Count -EQ 1)
{
    $siteID = $iisSites[0].Id
    $selectedSite = $iisSites[0]
    Write-Output "| The only IIS site defined on this machine was selected for investigation:"
}
else
{
    do {
        Write-Output "| What site are we investigating?..."
        Write-Output "| "
        $iisSites | Select-Object -Property Id, Name | Format-Table
        $siteID = Read-Host -Prompt ">   Select site ID we're investigating"
        $selectedSite = Get-WebSite | Where-Object -Property ID -EQ -Value "$siteID"
    } until ($selectedSite)
    Write-Output "| "
    Write-Output "| The following site is now selected:"
}
Write-Output "| "
Write-Output ([string]::Format("+   ID={0}    Name='{1}'", $selectedSite.Id, $selectedSite.Name))
Write-Output "| "
Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value ([string]::Format("+       Site-ID={0}    Name='{1}'", $selectedSite.Id, $selectedSite.Name))



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Select the IIS application being investigated" -PercentComplete 20



#====================================================================
#   Determine the IIS application that is being investigated
#--------------------------------------------------------------------

Write-Output "| "
Write-Output "| "
Write-Output "| "
if ($selectedSite.Collection.Count -LT 1)
{
    Write-Warning "There are no IIS applications defined under the selected site! This tool will now exit..."
    Remove-Item -Recurse $destinationTemp -Force
    exit
}
$applicationID = 0
$selectedApplication = $null
if ($selectedSite.Collection.Count -EQ 1)
{
    $selectedApplication = $selectedSite.Collection[0]
    $applicationPath = $selectedSite.Collection[0].Attributes["path"].Value
    $applicationPool = $selectedSite.Collection[0].Attributes["applicationPool"].Value
    Write-Output "| The only IIS application defined under the selected site is:"
}
else
{
    $iisSiteApplications = $selectedSite.Collection | Sort-Object -Property Path
    foreach ($app in $iisSiteApplications)
    {
        $app | Add-Member -MemberType NoteProperty -Name ID -Value $applicationID
        $applicationID = $applicationID + 1
    }
    $iisSiteApplications.Count
    do {
        Write-Output "| What application are we investigating?..."
        Write-Output "| "
        $iisSiteApplications | Select-Object -Property ID, Path, ApplicationPool | Format-Table
        $applicationID = Read-Host -Prompt ">   Select application we're investigating - type its ID"
        $selectedApplication = $iisSiteApplications | Where-Object -Property ID -EQ -Value "$applicationID"
    } until ($selectedApplication)
    $applicationPath = $selectedApplication.Attributes["path"].Value
    $applicationPool = $selectedApplication.Attributes["applicationPool"].Value
    Write-Output "| "
    Write-Output "| The following application is now selected:"
}
Write-Output "| "
Write-Output ([string]::Format("+   Path='{0}'    AppPool='{1}'", $applicationPath, $applicationPool))
Write-Output "| "
Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value ([string]::Format("+       Application-Path='{0}'    App-Pool-Name='{1}'", $applicationPath, $applicationPool))



Write-Output "| "
Write-Output "... collecting the data ..."
Write-Output "| "
Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       ...starting file collection..."
Add-Content -Path $fileCollectionLog -Value "|"



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Write down site and app" -PercentComplete 25



#====================================================================
#   Document IIS site and application being investigated
#--------------------------------------------------------------------

$destination = $destinationTemp + "Application-and-site.txt"
Add-Content -Path $destination -Value "|"
Add-Content -Path $destination -Value "|       Investigated IIS site"
Add-Content -Path $destination -Value "|       ---------------------"
Add-Content -Path $destination -Value "|"
Add-Content -Path $destination -Value ([string]::Format("+   ID={0}    Name='{1}'", $selectedSite.Id, $selectedSite.Name))
Add-Content -Path $destination -Value "|"
Add-Content -Path $destination -Value "|"
Add-Content -Path $destination -Value "|"
Add-Content -Path $destination -Value "|       Investigated app"
Add-Content -Path $destination -Value "|       ----------------"
Add-Content -Path $destination -Value "|"
Add-Content -Path $destination -Value ([string]::Format("+   Path='{0}'    AppPool='{1}'", $applicationPath, $applicationPool))
Add-Content -Path $destination -Value "|"
Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "Application-and-site.txt"



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Collecting HTTP.SYS configuration" -PercentComplete 30



#====================================================================
#   HTTP.SYS configuration
#--------------------------------------------------------------------

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       HTTP.SYS configuration..."

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "Cache Params" -PercentComplete 3
$destination = $destinationTemp + "netsh_http_show_cacheparam.txt"
& netsh http show cacheparam > $destination
Add-Content -Path $fileCollectionLog -Value "netsh http show cacheparam > netsh_http_show_cacheparam.txt"

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "Cache State" -PercentComplete 10
$destination = $destinationTemp + "netsh_http_show_cachestate.txt"
& netsh http show cachestate > $destination
Add-Content -Path $fileCollectionLog -Value "netsh http show cachestate > netsh_http_show_cachestate.txt"

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "IP Listening" -PercentComplete 25
$destination = $destinationTemp + "netsh_http_show_iplisten.txt"
& netsh http show iplisten > $destination
Add-Content -Path $fileCollectionLog -Value "netsh http show iplisten > netsh_http_show_iplisten.txt"

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "Services, Service State" -PercentComplete 40
$destination = $destinationTemp + "netsh_http_show_servicestate.txt"
& netsh http show servicestate > $destination
Add-Content -Path $fileCollectionLog -Value "netsh http show servicestate > netsh_http_show_servicestate.txt"

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "Settings" -PercentComplete 50
$destination = $destinationTemp + "netsh_http_show_setting.txt"
& netsh http show setting > $destination
Add-Content -Path $fileCollectionLog -Value "netsh http show setting > netsh_http_show_setting.txt"

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "SSL Certificates" -PercentComplete 65
$destination = $destinationTemp + "netsh_http_show_sslcert.txt"
& netsh http show sslcert > $destination
Add-Content -Path $fileCollectionLog -Value "netsh http show sslcert > netsh_http_show_sslcert.txt"

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "Timeouts" -PercentComplete 80
$destination = $destinationTemp + "netsh_http_show_timeout.txt"
& netsh http show timeout > $destination
Add-Content -Path $fileCollectionLog -Value "netsh http show timeout > netsh_http_show_timeout.txt"

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "URL ACLs" -PercentComplete 90
$destination = $destinationTemp + "netsh_http_show_urlacl.txt"
& netsh http show urlacl > $destination
Add-Content -Path $fileCollectionLog -Value "netsh http show urlacl > netsh_http_show_urlacl.txt"

Write-Progress -Id 2 -Activity "HTTP.SYS configuration" -Status "Done" -Completed



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "IIS Configuration files" -PercentComplete 35



#====================================================================
#   IIS setup and configuration files
#--------------------------------------------------------------------
#   TODO: the applicationHost.config file might be placed on a network location if IIS Shared Configuration is used.
#   TODO: Probably the IISAdministration PowerShell module can retrieve the actual config file/location; the following is just the default location.

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       IIS setup and configuration files..."
$destination = $destinationTemp + "\CBS.log"
$sourceFiles = $Env:SystemRoot + "\Logs\CBS\cbs.log"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Destination $destination
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}
$destination = $destinationTemp + "\IIS-install.log"
$sourceFiles = $Env:SystemRoot + "\iis.log"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Destination $destination
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}
$destination = $destinationTemp + "\IIS-Config\"
$sourceFiles = $Env:SystemRoot + "\System32\inetsrv\config\*.config"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Destination (New-Item -ItemType Directory -Force $destination)
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}
$destination = $destinationTemp + "\IIS-Config\Schema\"
$sourceFiles = $Env:SystemRoot + "\System32\inetsrv\config\Schema\*.xml"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Destination (New-Item -ItemType Directory -Force $destination)
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Windows Events log entries" -PercentComplete 40



#====================================================================
#   Windows Events: System, Application, Security log entries
#--------------------------------------------------------------------

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       Windows Events: System, Application, Security log entries..."
$millisecondsBack = $daysBackInTime * 24 * 60 * 60 * 1000

Write-Progress -Id 2 -Activity "Windows Events" -Status "System log" -PercentComplete 3
$destination = $destinationTemp + "\WinEvents-System.evtx"
& wevtutil export-log System $destination /query:"*[System[TimeCreated[timediff(@SystemTime) <= $millisecondsBack]]]"
Add-Content -Path $fileCollectionLog -Value "WinEvents-System.evtx"

Write-Progress -Id 2 -Activity "Windows Events" -Status "Application log" -PercentComplete 30
$destination = $destinationTemp + "\WinEvents-Application.evtx"
& wevtutil export-log Application $destination /query:"*[System[TimeCreated[timediff(@SystemTime) <= $millisecondsBack]]]"
Add-Content -Path $fileCollectionLog -Value "WinEvents-Application.evtx"

Write-Progress -Id 2 -Activity "Windows Events" -Status "Security log" -PercentComplete 60
$destination = $destinationTemp + "\WinEvents-Security.evtx"
& wevtutil export-log Security $destination /query:"*[System[TimeCreated[timediff(@SystemTime) <= $millisecondsBack]]]"
Add-Content -Path $fileCollectionLog -Value "WinEvents-Security.evtx"

Write-Progress -Id 2 -Activity "Windows Events" -Status "Setup log" -PercentComplete 70
$destination = $destinationTemp + "\WinEvents-Setup.evtx"
& wevtutil export-log Setup $destination /query:"*[System[TimeCreated[timediff(@SystemTime) <= $millisecondsBack]]]"
Add-Content -Path $fileCollectionLog -Value "WinEvents-Setup.evtx"

Write-Progress -Id 2 -Activity "Windows Events" -Status "Done" -Completed



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "HTTP.SYS logs, HTTPERR" -PercentComplete 50



#====================================================================
#   HTTP.SYS log files
#--------------------------------------------------------------------

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       HTTP.SYS log files..."
$sourcePath = Get-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Services\HTTP\Parameters\" | Select-Object -Expand ErrorLoggingDir
if (-not($sourcePath))
{
    $sourcePath = $Env:SystemRoot + "\System32\LogFiles\HTTPERR\"
}
else
{
    $sourcePath = $sourcePath.Replace("%SystemDrive%", $Env:SystemDrive)
    $sourcePath = $sourcePath.Replace("%SystemRoot%", $Env:SystemRoot)
}
if (Test-Path ($sourcePath))
{
    $sourceFiles = Get-ChildItem -Path $sourcePath | Where-Object { $_.LastWriteTime -GE $startingDate }
    if ($sourceFiles -and ($sourceFiles.Length -gt 0))
    {
        $destination = $destinationTemp + "HTTPSYS-HTTPERR\"
        New-Item -ItemType Directory -Force $destination | Out-Null
        $index = 0
        foreach($file in $sourceFiles)
        {
            $index++
            Write-Progress -Id 2 -Activity "HTTPERR logs" -Status $file.Name -PercentComplete (($index/$sourceFiles.Count)*100)
            Copy-Item -Path $file.FullName -Destination $destination
            Add-Content -Path $fileCollectionLog -Value $file.FullName
        }
        Write-Progress -Id 2 -Activity "HTTPERR logs" -Status "Done" -Completed
    }
}



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "IIS log files" -PercentComplete 55



#====================================================================
#   IIS log files
#--------------------------------------------------------------------

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       IIS log files..."
$sourcePath = $selectedSite.LogFile.Directory
if(-not($sourcePath))
{
    $sourcePath = $Env:SystemDrive + "\inetpub\logs\LogFiles\W3SVC" + $siteID
}
else
{
    $sourcePath = $sourcePath.Replace("%SystemDrive%", $Env:SystemDrive)
    $sourcePath = $sourcePath.Replace("%SystemRoot%", $Env:SystemRoot)
    $sourcePath = $sourcePath + "\W3SVC" + $siteID
}
if (Test-Path $sourcePath)
{
    $sourceFiles = Get-ChildItem -Path $sourcePath | Where-Object { $_.LastWriteTime -GE $startingDate }
    if ($sourceFiles -and ($sourceFiles.Length -gt 0))
    {
        $destination = $destinationTemp + "\IIS-Logs-W3SVC" + $siteID + "\"
        New-Item -ItemType Directory -Force $destination | Out-Null
        $index = 0
        foreach($file in $sourceFiles)
        {
            $index++
            Write-Progress -Id 2 -Activity "IIS logs" -Status $file.Name -PercentComplete (($index/$sourceFiles.Count)*100)
            Copy-Item -Path $file.FullName -Destination $destination
            Add-Content -Path $fileCollectionLog -Value $file.FullName
        }
        Write-Progress -Id 2 -Activity "IIS logs" -Status "Done" -Completed
    }
}



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "FREB, Failed Request Tracing" -PercentComplete 65



#====================================================================
#   FREB, Failed Request Tracing log files
#--------------------------------------------------------------------

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       FREB, Failed Request Tracing log files..."
$sitesConfigSection = Get-IISConfigSection -SectionPath "system.applicationHost/sites"
$sitesConfigCollection = Get-IISConfigCollection -ConfigElement $sitesConfigSection
$siteConfigElement = Get-IISConfigCollectionElement -ConfigCollection $sitesConfigCollection -ConfigAttribute @{"id" = "$siteID"}
$siteFrebConfigElement = Get-IISConfigElement -ConfigElement $siteConfigElement -ChildElementName "traceFailedRequestsLogging"
$sourcePath = Get-IISConfigAttributeValue -ConfigElement $siteFrebConfigElement -AttributeName "directory"
if (-not($sourcePath))
{
    $sourcePath = $Env:SystemDrive + "\inetpub\logs\FailedReqLogFiles\W3SVC" + $siteID
}
else
{
    $sourcePath = $sourcePath + "\W3SVC" + $siteID
    $sourcePath = $sourcePath.Replace("%SystemDrive%", $Env:SystemDrive)
    $sourcePath = $sourcePath.Replace("%SystemRoot%", $Env:SystemRoot)
}
if (Test-Path $sourcePath)
{
    $sourceFiles = Get-ChildItem -Path $sourcePath | Where-Object { ($_.LastWriteTime -GE $startingDate) -OR ($_.Extension -EQ ".xsl") }
    if ($sourceFiles -and ($sourceFiles.Length -gt 0))
    {
        $destination = $destinationTemp + "\IIS-FREB-W3SVC" + $siteID + "\"
        New-Item -ItemType Directory -Force $destination | Out-Null
        $index = 0
        foreach($file in $sourceFiles)
        {
            $index++
            Write-Progress -Id 2 -Activity "FREB logs" -Status $file.Name -PercentComplete (($index/$sourceFiles.Count)*100)
            Copy-Item -Path $file.FullName -Destination $destination
            Add-Content -Path $fileCollectionLog -Value $file.FullName
        }
        Write-Progress -Id 2 -Activity "FREB logs" -Status "Done" -Completed
    }
}



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status ".NET and ASP.NET configuration" -PercentComplete 70



#====================================================================
#   Root .NET and ASP.NET configuration
#--------------------------------------------------------------------
#   We're interested mostly in .NET machine.config and the root Web.config

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       Root .NET and ASP.NET configuration..."

Write-Progress -Id 2 -Activity ".NET FX Config" -Status "x64 v2.0.50727" -PercentComplete 3
$destination = $destinationTemp + "\.NET-FX64v2.0.50727-Config\"
$sourceFiles = $Env:SystemRoot + "\Microsoft.NET\Framework64\v2.0.50727\Config\*.*"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Recurse -Destination (New-Item -ItemType Directory -Force $destination)
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}

Write-Progress -Id 2 -Activity ".NET FX Config" -Status "x64 v4.0.30319" -PercentComplete 25
$destination = $destinationTemp + "\.NET-FX64v4.0.30319-Config\"
$sourceFiles = $Env:SystemRoot + "\Microsoft.NET\Framework64\v4.0.30319\Config\*.*"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Recurse -Destination (New-Item -ItemType Directory -Force $destination)
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}

Write-Progress -Id 2 -Activity ".NET FX Config" -Status "x32 v2.0.50727" -PercentComplete 50
$destination = $destinationTemp + "\.NET-FX32v2.0.50727-Config\"
$sourceFiles = $Env:SystemRoot + "\Microsoft.NET\Framework\v2.0.50727\Config\*.*"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Recurse -Destination (New-Item -ItemType Directory -Force $destination)
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}

Write-Progress -Id 2 -Activity ".NET FX Config" -Status "x32 v4.0.30319" -PercentComplete 75
$destination = $destinationTemp + "\.NET-FX32v4.0.30319-Config\"
$sourceFiles = $Env:SystemRoot + "\Microsoft.NET\Framework\v4.0.30319\Config\*.*"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Recurse -Destination (New-Item -ItemType Directory -Force $destination)
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}

Write-Progress -Id 2 -Activity ".NET FX Config" -Status "Done" -Completed



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Application's effective runtime configuration" -PercentComplete 75



#====================================================================
#   The effective configuration at runtime of the application
#--------------------------------------------------------------------

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       The effective configuration at runtime of the application..."
$destination = $destinationTemp + "\App-effective-configuration\"
$sourceFiles = $Env:SystemDrive + "\inetpub\temp\appPools\" + $applicationPool + "\*.*"
if (Test-Path $sourceFiles)
{
    Copy-Item -Path $sourceFiles -Recurse -Destination (New-Item -ItemType Directory -Force $destination)
    Add-Content -Path $fileCollectionLog -Value $sourceFiles
}



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Packaging all in a ZIP archive" -PercentComplete 80



#====================================================================
#   About system configuration, installed .NET versions
#--------------------------------------------------------------------

Add-Content -Path $fileCollectionLog -Value "|"
Add-Content -Path $fileCollectionLog -Value "+       The system configuration, installed .NET versions..."
Write-Progress -Id 2 -Activity "System info" -Status "Summary file" -PercentComplete 20
$destination = $destinationTemp + "System-Info.txt"
Get-ComputerInfo > $destination
Add-Content -Path $fileCollectionLog -Value $destination
Write-Progress -Id 2 -Activity ".NET Framework" -Status "version keys" -PercentComplete 70
$destination = $destinationTemp + "NET-Framework-versions.reg.txt"
& Reg.exe export "HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP" $destination
Add-Content -Path $fileCollectionLog -Value $destination
Write-Progress -Id 2 -Activity ".NET Core" -Status "version info" -PercentComplete 90
$destination = $destinationTemp + "NET-Core-versions.txt"
{ & DotNet.exe --info > $destination } -ErrorAction SilentlyContinue
Add-Content -Path $fileCollectionLog -Value $destination



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Packaging all in a ZIP archive" -PercentComplete 90



#====================================================================
#   Now compress the collected files in a ZIP
#--------------------------------------------------------------------
#   NOTE: The Compress-Archive is only available with the more recent versions of PowerShell
#   INCOMPAT: Compress-Archive -Path C:\Windows\System32\inetsrv\Config\*.config -DestinationPath $destinationArchiveFileName
$dateTimeFormatted = [string]::Format("{0:yyyy-MM-dd_hh-mm-ss}",$today)
$destinationFile = (Get-Item -Path $destinationRoot).FullName + "\IIS-ConfigAndLogs_" + $dateTimeFormatted + ".zip"
if (Test-Path $destinationFile) { Remove-item $destinationFile -Force }
Add-Type -Assembly "System.IO.Compression.FileSystem"
[IO.Compression.ZipFile]::CreateFromDirectory((Get-Item -Path $destinationTemp), $destinationFile)
Remove-Item -Recurse $destinationTemp -Force
Write-Output "| "
Write-Output "| "
Write-Output "| "
Write-Output "| The relevant logs were collected and zipped. The archive file was successfully saved:"
Write-Output "| "
Write-Output ([string]::Format("+   {0}", $destinationFile))
Write-Output "| "
Write-Output "| "
Write-Output "| "
Write-Output "| DONE"
Write-Output "| "



Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Packaging all in a ZIP archive" -PercentComplete 99
Write-Progress -Id 1 -Activity "LogCatcher Steps" -Status "Done" -Completed

