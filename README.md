# IIS-TS-Grabber

A PowerShell script to help automate diagnosing data gathering when troubleshooting IIS applications or sites.

## Usage

This script needs to run with Administrative rights, since it will access files under C:\Windows\System32 (IIS configuration files).

You might need to relax the PowerShell execution policy, for the console instance where you're going to execute the script:

`Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process`

## Rationale

When troubleshooting or simply assisting with diagnose, we might spend a lot of time simply gathering the logs, configuration, Windows events. This script helps save that time.

This script is a companion of the article at http://linqto.me/IIS-Basic-Files.
For those who'd prefer to be in control, simply following the steps in that article should lead to almost same result.

The article at http://linqto.me/IisTS helps explain why we collect this data, how to think when troubleshooting.